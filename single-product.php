<?php
require_once("lib/header.php");
require_once("view/admin/vendor/autoload.php");
use App\Auth\connect;

$id = $_GET['sl'];

$singlePro = new connect;
$data = $singlePro->getProductById($id);
?>



    <div class="products-single">
      <div class="container">
         <div class="row">
           <div class="col-sm-12 col-sm-12 col-xs-12">
             <div class="single-img">
               <img src="assets/img/products/<?php echo $data['pro_img'];?>" class="img-responsive" alt="product image">
             </div>
           </div>
           <div class="col-sm-12 col-sm-12 col-xs-12">
            <p class="products-single-name">Name: <?php echo $data['name'];?></p>
            <p class="products-single-details">Details: <?php echo $data['description'];?> </p>
           </div>
         </div>                  
      </div>
    </div>


<?php
require_once("lib/footer.php");
?>