/* 
// Designer & Developer: M Nazim Uddin Molla;
// E-mail: mohnazimuddin@gmail.com;
// Phone: +8801711062063, +8801515243401;
// Facebook: www.facebook.com/nzm404;
// Gitlab: https://gitlab.com/mnazimmolla;
// Github: https://github.com/mnazimmolla;
*/  
$(window).on('load', function() { // makes sure the whole site is loaded 
  $('#status').fadeOut(); // will first fade out the loading animation 
  $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  $('body').delay(350).css({'overflow':'visible'});
})