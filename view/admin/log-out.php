<?php session_start();?>
<?php
	if(isset($_SESSION['email']))
	{
		unset($_SESSION['email']);
		session_destroy();
		header("location: login.php");
	}
?>