<?php
namespace App\Auth;
use PDO;

class connect 
{
	public $connection;
	private $host = 'localhost';
	private $dbname ='smtrade';
	private $dbuser = 'root';
	private $dbpass = '';

	public function __construct ()
	{
	  return $this->connection = new PDO("mysql:host=$this->host; dbname=$this->dbname", $this->dbuser, $this->dbpass);
	}

	public function getCategories()
	{
		$categories = $this->connection->prepare("SELECT * FROM categories");
		$categories->execute();
		$data = $categories->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}
	public function getCategoriesbyDesc()
	{
		$categories = $this->connection->prepare("SELECT * FROM categories ORDER BY id_uni_cat DESC");
		$categories->execute();
		$data = $categories->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}	
	public function saveCategory($data)
	{
		$cat_id = $data['cat_id'];
		$category_name = $data['category_name'];

		$saveCat = $this->connection->prepare("INSERT INTO categories (cat_name, cat_id) VALUES (:cat_name, :cat_id)");
		$saveCat->bindParam(':cat_name', $category_name);
		$saveCat->bindParam(':cat_id', $cat_id);
		$success = $saveCat->execute();
		if($success)
		{
			header("location: ../view-categories.php");
		}
		else {
			die('die');
		}
	}
	public function getCatById($id)
	{
		$id = $id;
		$get = $this->connection->prepare("SELECT * from categories where id_uni_cat=:id");
		$get->bindParam(':id', $id);
		$get->execute();
		$data = $get->fetch(PDO::FETCH_ASSOC);
		return $data;
	}
	public function editCategory($data)
	{

		$id = $data['id'];
		$cat_name = $data['cat_name'];
		$cat_id = $data['cat_id'];

		$edit = $this->connection->prepare("UPDATE categories SET cat_name=:cat_name, cat_id=:cat_id WHERE id_uni_cat=:id");
		$edit->bindParam(':cat_name', $cat_name);
		$edit->bindParam(':cat_id', $cat_id);
		$edit->bindParam(':id', $id);
		$save = $edit->execute();

		if($save)
		{
			header("location: ../view-categories.php");
		}
		else {
			die('failed');
		}

	}
	public function getCatnameByid($id='')
	{	
		$id = $id;
		$cat_name = $this->connection->prepare("SELECT cat_name FROM categories WHERE id_uni_cat=:id");
		$cat_name->bindParam(':id', $id);
		$cat_name->execute();
		$data = $cat_name->fetch(PDO::FETCH_ASSOC);
		return $data;
	}
	public function deleteCategory($id)
	{
		$id = $id;
		$delete = $this->connection->prepare("DELETE FROM categories WHERE id_uni_cat=:id");
		$delete->bindParam(':id', $id);
		$success = $delete->execute();
		if($success)
		{
			header("location: view-categories.php");
		}
		else
		{
			die("failed");
		}
	}
	public function saveProduct($data='')
	{	
		$name = $data['name'];
		$cat_id = $data['cat_id'];
		$pro_desc = $data['pro-desc'];
		$pro_img = $_FILES['product_img']['name'];

		$pro_temp_name = $_FILES['product_img']['tmp_name'];

		$checkExt = substr($pro_img, strpos($pro_img, ".") +1);
        $validExt = array('jpg','png','gif','jpeg');

        $path = "../../../assets/img/products/";

       if(!in_array($checkExt, $validExt))
       {
           die("Invalid File Format");
       }
       else
       {
           move_uploaded_file($pro_temp_name, $path.$pro_img);
       }  
       $save = $this->connection->prepare("INSERT INTO products (cat_id, name, description, pro_img) VALUES (:cat_id, :name, :description, :pro_img)");
       $save->bindParam(':cat_id', $cat_id);      
       $save->bindParam(':name', $name);      
       $save->bindParam(':description', $pro_desc);      
       $save->bindParam(':pro_img', $pro_img);
       $success =  $save->execute();

       if($success)
       {
       	header("location: ../view-products.php");
       }
       else {
       	echo 'try later';
       }
	}
	public function editProductById($data='')
	{

		$name = $data['name'];
		$cat_id = $data['cat_id'];
		$pro_desc = $data['pro-desc'];
		$id = $data['id'];
		$pro_img = $_FILES['product_img']['name'];
		$pro_temp_name = $_FILES['product_img']['tmp_name'];

		$checkExt = substr($pro_img, strpos($pro_img, ".") +1);
        $validExt = array('jpg','png','gif','jpeg');

        $path = "../../../assets/img/products/";

        move_uploaded_file($pro_temp_name, $path.$pro_img);
       if($pro_temp_name == null)
       {
	       $save = $this->connection->prepare("UPDATE products SET name=:name, cat_id=:cat_id, description=:pro_desc WHERE id=:id");
	       $save->bindParam(':name', $name);  
	       $save->bindParam(':cat_id', $cat_id);  
	       $save->bindParam(':pro_desc', $pro_desc);  
	       $save->bindParam(':id', $id);
	       $success = $save->execute();  
       } 
       else 
       {
	       $save = $this->connection->prepare("UPDATE products SET name=:name, cat_id=:cat_id, description=:pro_desc, pro_img=:pro_img WHERE id=:id");
	       $save->bindParam(':name', $name);  
	       $save->bindParam(':cat_id', $cat_id);  
	       $save->bindParam(':pro_desc', $pro_desc);  
	       $save->bindParam(':pro_img', $pro_img);
	       $save->bindParam(':id', $id);
	       $success = $save->execute();  
		}
		if($success)
		{
			header("location: ../view-products.php");
		}
		else 
		{
			die("product update failed!");
		}
	}
	public function getProducts()
	{
		$data = $this->connection->prepare("SELECT * FROM products LEFT JOIN categories ON categories.id_uni_cat = products.cat_id ORDER BY products.id DESC");
		$data->execute();
		$success = $data->fetchAll(PDO::FETCH_ASSOC);
		return $success;
	}
	public function getProductByCat($id)
	{
		$id= $id;
		$data = $this->connection->prepare("SELECT * from products WHERE cat_id = :cat_id");
		$data->bindParam(':cat_id', $id);
		$data->execute();
		$success = $data->fetchAll(PDO::FETCH_ASSOC);
		return $success;
	}
	public function getProductsCatID($id='')
	{
		$id = $id;
		$data = $this->connection->prepare("SELECT categories.cat_id, categories.id_uni_cat, products.id, products.id, products.name, products.description, products.pro_img from categories LEFT JOIN products ON categories.id_uni_cat = products.cat_id WHERE categories.id_uni_cat=products.cat_id AND categories.cat_id =:cat_id");
		$data->bindParam(':cat_id', $id);
		$data->execute();
		$success = $data->fetchAll(PDO::FETCH_ASSOC);
		return $success;

	}
	public function getProductById($id)
	{
		$id = $id;
		$data = $this->connection->prepare("SELECT * FROM products WHERE id=:id");
		$data->bindParam(":id", $id);
		$data->execute();
		$result = $data->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	public function deleteProductbyId($id='')
	{
		$id = $id;
		$delete = $this->connection->prepare("DELETE FROM products WHERE id=:id");
		$delete->bindParam(':id', $id);
		$success = $delete->execute();
		if($success)
		{
			header("location: view-products.php");
		}
	}

}


?>