<?php session_start(); ?>
<?php
    if(!isset($_SESSION['email']))
    {
        header("location: ../login.php");
    }
?>
<?php
use App\Auth\connect;
require_once('../vendor/autoload.php');

	if($_POST)
	{
	 	$cat_id = $_POST["cat_id"];
	 	$category_name = $_POST["category_name"];

	 	if($cat_id == null or $category_name == null)
	 	{
	 		header("location: ../create-category.php");
	 	}
	 	else {
	 		$save = new connect;
	 		$save->saveCategory($_POST);
	 	}
	}


?>