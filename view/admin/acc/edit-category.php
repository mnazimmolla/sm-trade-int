<?php session_start(); ?>
<?php
    if(!isset($_SESSION['email']))
    {
        header("location: ../login.php");
    }
?>
<?php
use App\Auth\connect;
require_once('../vendor/autoload.php');

	if($_POST)
	{
	 	$cat_id = $_POST["cat_id"];
	 	$cat_name = $_POST["cat_name"];

	 	if($cat_id == null or $cat_name == null)
	 	{
	 		header("location: ../create-category.php");
	 	}
	 	else {
	 		$save = new connect;
	 		$save->editCategory($_POST);
	 	}
	}


?>