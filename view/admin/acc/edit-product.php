<?php session_start(); ?>
<?php
    if(!isset($_SESSION['email']))
    {
        header("location: ../login.php");
    }
?>
<?php
	require_once('../vendor/autoload.php');
	use App\Auth\connect;
?>

<?php
	if($_POST)
	{

		$name = $_POST['name'];
		$cat_id = $_POST['cat_id'];
		$pro_desc = $_POST['pro-desc'];
		$id = $_POST['id'];

		if($name == null or $cat_id == null or $pro_desc == null)
		{
			header("location: ../edit-product.php?id=" . "$id");
		}
		else
		{
			$data = new connect;
			$data->editProductById($_POST);
		}
	}

?>