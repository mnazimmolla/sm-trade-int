<?php session_start(); ?>
<?php
    if(!isset($_SESSION['email']))
    {
        header("location: ../login.php");
    }
?>
<?php
use App\Auth\connect;
require_once('../vendor/autoload.php');

	if($_POST)
	{
	 	$pro_name = $_POST["name"];
	 	$pro_cat = $_POST["cat_id"];
	 	$pro_desc = $_POST["pro-desc"];

	 	if($pro_name == null or $pro_cat == null or $pro_desc == null)
	 	{
	 		header("location: ../create-product.php");
	 	}
	 	else {
	 		$save = new connect;
	 		$save->saveProduct($_POST);
	 	}
	}


?>