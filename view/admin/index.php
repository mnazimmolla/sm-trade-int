<?php session_start(); ?>
<?php
    if(!isset($_SESSION['email']))
    {
        header("location: login.php");
    }
?>
<?php
require_once("lib/header.php");
?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
            </div>
        </div>

<?php
require_once("lib/footer.php");
?>

