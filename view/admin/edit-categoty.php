<?php session_start(); ?>
<?php
    if(!isset($_SESSION['email']))
    {
        header("location: login.php");
    }
?>
<?php 
require_once('lib/header.php');
require_once('vendor/autoload.php');
use App\Auth\connect;

$id = $_GET['id'];

$get_cat_by_id = new connect;
$data = $get_cat_by_id->getCatById($id);

?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Category</h1>
                    <form method="POST" action="acc/edit-category.php">
					  <div class="form-group">
					    <label for="category">Main Categories</label>
					    <select name="cat_id" id="category" class="form-control">
					    	
					    	<option value="<?php echo $data['cat_id'];?>">
					    	<?php 
					    		if($data['cat_id']==3)
					    		{
					    			echo 'Garments';
					    		}
					    		elseif($data['cat_id']==2)
					    		{
					    			echo 'Vegetables';
					    		}
					    		else
					    		{
					    			echo 'Food';
					    		}
					    	?>	
					    	</option>
					    	<option value="1">Food</option>
					    	<option value="2">Vegetables</option>
					    	<option value="3">Garments</option>

						</select>
					  </div>
					  <div class="form-group">
					    <label for="cat_name">Category Name</label>
					    <input type="text" value="<?php echo $data['cat_name'];?>" id="cat_name" name="cat_name" class="form-control">
					  </div>
					  <input type="hidden" name="id" value="<?php echo $data['id_uni_cat'];?>">
					  <button type="submit" class="btn btn-default">Save Category</button>
					</form>                    
                </div>
            </div>
        </div>
      

<?php 
require_once('lib/footer.php');
?>