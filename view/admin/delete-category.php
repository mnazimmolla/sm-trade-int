<?php session_start(); ?>

<?php
    if(!isset($_SESSION['email']))
    {
        header("location: login.php");
    }
?>

<?php
	require_once("vendor/autoload.php");
	use  App\Auth\connect;
?>

<?php
	$id = $_GET["id"];
	$del = new connect;
	$del->deleteCategory($id);
?>