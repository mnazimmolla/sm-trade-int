<?php session_start(); ?>
<?php
    if(!isset($_SESSION['email']))
    {
        header("location: login.php");
    }
?>
<?php 
require_once('lib/header.php');
require_once('vendor/autoload.php');
use App\Auth\connect;

$id = $_GET['id'];

$pro_by_id = new connect;
$data = $pro_by_id->getProductById($id);

?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Create Product</h1>
                    <form method="POST" action="acc/edit-product.php" enctype="multipart/form-data">
					  <div class="form-group">
					    <label for="name">Product Name</label>
					    <input type="text" value="<?php echo $data['name'];?>" class="form-control" name="name" id="name" placeholder="Product Name">
					  </div>
					  <div class="form-group">
					    <label for="category">Category</label>
					    <select name="cat_id" value="" class="form-control">

					    	<?php
					    		$id_for_cat = $data['cat_id'];
					    		$cat_name= new connect;
					    		$get_cat_name = $cat_name->getCatnameByid($id_for_cat);
					    	?>

						<option selected="selected" value="<?php echo $data['cat_id'];?>">
							<?php echo $get_cat_name['cat_name'];?>
						</option>

					    <?php
					    	$cat = new connect;
					    	$cat = $cat->getCategories();

					    	foreach ($cat as $key => $value) 
					    	{ ?>
					    		<option value="<?php echo $value['id_uni_cat'];?>"><?php echo $value['cat_name']; ?></option>

					    <?php	}
					    ?>


						</select>
						  	
					  </div>
					  <div class="form-group">
					  	<label for="pro-desc">Product Descrioption</label>
					  	<textarea name="pro-desc" id="pro-desc" class="form-control" rows="5"><?php echo $data['description'];?></textarea>
					  </div>
					  <div class="form-group">
					    <label for="pro-image">Product Image</label>
					    <input type="file" id="pro-image" name="product_img" class="form-control">
					  </div>
					  <input type="hidden" value="<?php echo $data['id'];?>" name="id">
					  <button type="submit" class="btn btn-default">Save Product</button>
					</form>
                </div>
            </div>
        </div>
      

<?php 
require_once('lib/footer.php');
?>