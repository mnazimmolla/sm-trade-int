<?php session_start(); ?>
<?php
    if(!isset($_SESSION['email']))
    {
        header("location: login.php");
    }
?>
<?php 
require_once('lib/header.php');
require_once('vendor/autoload.php');
use App\Auth\connect;
?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Create Category</h1>
                    <form method="POST" action="acc/create-category.php">
					  <div class="form-group">
					    <label for="category">Main Categories</label>
					    <select name="cat_id" id="category" class="form-control">
					    	<option >Select Main Category</option>
					    	<option value="1">Food</option>
					    	<option value="2">Vegetables</option>
					    	<option value="3">Garments</option>
						</select>
					  </div>
					  <div class="form-group">
					    <label for="category_name">Category Name</label>
					    <input type="text" id="category_name" name="category_name" class="form-control">
					  </div>
					  <button type="submit" class="btn btn-default">Save Category</button>
					</form>
                </div>
            </div>
        </div>
      

<?php 
require_once('lib/footer.php');
?>