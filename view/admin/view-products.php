<?php session_start(); ?>
<?php
    if(!isset($_SESSION['email']))
    {
        header("location: login.php");
    }
?>
<?php
require_once('lib/header.php');
require_once('vendor/autoload.php');
use App\Auth\connect;
$Products = new connect;
$data = $Products->getProducts();


?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Products</h1>
					<table class="table table-striped">
					  <tr>
					  	<td>Sl.</td>
					  	<td>Image</td>
					  	<td>Name</td>
					  	<td>Category</td>
					  	<td>Description</td>
					  	<td>Edit</td>
					  	<td>Delete</td>
					  </tr>	
					  
					  <?php
					  $srl = 1;
					  	foreach ($data as $key => $value) { ?>
						 <tr>
						  	<td><?php echo $srl++;?></td>
						  	<td><img style="max-width: 40px; max-height: 40px;" src="../../assets/img/products/<?php echo $value['pro_img'];?>" alt="product image"></td>
						  	<td><?php echo $value['name'];?></td>
						  	<td><?php echo $value['cat_name'];?></td>
						  	<td><?php echo $value['description'];?></td>
						  	<td><a href="edit-product.php?id=<?php echo $value['id'];?>">Edit</a></td>
						  	<td><a class="text-danger" href="delete-products.php?id=<?php echo $value['id'];?>">Delete</a></td>
						 </tr>		
					 <?php 	}
					  ?>



					</table>
                </div>
            </div>
        </div>
      

<?php
require_once('lib/footer.php');
?>