<?php session_start(); ?>
<?php
    if(!isset($_SESSION['email']))
    {
        header("location: login.php");
    }
?>
<?php
require_once('lib/header.php');
require_once('vendor/autoload.php');
use App\Auth\connect;
$cat = new connect;
$data = $cat->getCategoriesbyDesc();

?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Categories</h1>
					<table class="table table-striped">
						<tr>
						  	<td>Sl.</td>
						  	<td>Category</td>
						  	<td>Main Category</td>
						  	<td>Edit</td>
						  	<td>Delete</td>
						</tr>	

						<?php
							$sl = 1;
							foreach ($data as $key => $value) { ?>
						<tr>
						  	<td><?php echo $sl++;?></td>
						  	<td><?php echo $value['cat_name'];?></td>
						  	<td>
						  	<?php 
						  		if($value['cat_id']==3)
						  		{
						  			echo 'Garments';
						  		}
						  		elseif($value['cat_id']==2)
						  		{
						  			echo 'Vegetables';
						  		}
						  		else {
						  			echo 'Food';
						  		}
						  	 ?></td>
						  	
						  	<td</td>
						  	<td><a href="edit-categoty.php?id=<?php echo $value["id_uni_cat"];?>">Edit</a></td>
						  	<td><a class="text-danger" href="delete-category.php?id=<?php echo $value["id_uni_cat"];?>">Delete</a></td>
						</tr>							
						<?php		
							}
						?>



					</table>
                </div>
            </div>
        </div>
      

<?php
require_once('lib/footer.php');
?>