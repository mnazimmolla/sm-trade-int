<?php
require_once("lib/header.php");
require_once("view/admin/vendor/autoload.php");
use App\Auth\connect;
$id = $_GET['br'];
$products = new connect;
$pro_by_cat = $products->getProductsCatID($id);

?>

    <div class="products">
      <div class="container">
        <div class="row">

          <?php 
            foreach($pro_by_cat as $key => $value)
            { ?>
         
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="product-item">
              <div class="product-image">
                <img src="assets/img/products/<?php echo $value['pro_img'];?>" class="img-responsive">
              </div>
              <div class="product-details">
                <p class="product-name">Name: <?php echo $value['name'];?></p>
                <p class="product-detail">Details: <?php echo $value['description'];?></p>
              </div>
              <button onclick="location.href = 'single-product.php?sl=<?php echo $value['id'];?>';" class="btn">Details</button>
            </div>             
          </div>
            
            <?php } ?>   

        </div>
      </div>                           
    </div>


<?php
require_once("lib/footer.php");
?>