<?php
require_once("lib/header.php");
?>
   	<div class="slider">
   		<div class="row">
   			<div class="container-fluid">
				<!-- <div class="codrops-top clearfix">
					<a class="codrops-icon codrops-icon-prev" href="http://tympanus.net/Development/InlineAnchorStyles/"><span>Previous Demo</span></a>
					<span class="right"><a class="codrops-icon codrops-icon-drop" href="http://tympanus.net/codrops/?p=19242"><span>Back to the Codrops Article</span></a></span>
				</div> -->
				<div id="boxgallery" class="boxgallery" data-effect="effect-1">
					<div class="panel"><img src="img/1.jpg" alt="Image 1"/></div>
					<div class="panel"><img src="img/2.jpg" alt="Image 2"/></div>
					<div class="panel"><img src="img/3.jpg" alt="Image 3"/></div>
					<div class="panel"><img src="img/4.jpg" alt="Image 4"/></div>
				</div>
				<header class="codrops-header">
					<h3>SM Trade International</h3>
					<p>Lorem Ipsum Dolor imit Lorem Ipsum Dolor imit</p>
					<nav class="codrops-demos">
						<a href="#">SM Trade</a>
						<a href="#">Musco IT</a>
						<a href="#">Musco Food</a>
					</nav>
				</header>				   				
   			</div>   			
   		</div>
   	</div>

   	<div class="clearfix">
   		
   	</div>

   	<div class="business-promotion">

   		<div class="row">
   			<div class="container">
   				<h3 class="text-center">We  Beleive in</h3>
   				<div class="promotion-item">
   					<div class="row">
   						<div class="col-md-5 col-xs-12 col-sm-5">
   							<div class="promotion-img">
   								<img src="http://img-aws.ehowcdn.com/340x221p/photos.demandstudios.com/getty/article/81/145/80609557.jpg" class="img-responsive" alt="Business Promotion Image">
   							</div>
   						</div>
   						<div class="col-md-7 col-xs-12 col-sm-7">
   							<div class="promotion-desc">
   								<p class="text-justify">
SM Trade International is the leading exporter of Apparel, Garments, Food Product, Fresh Fruit & Vegetable and globally recognized Buying and Sourcing House of apparels and garments in Bangladesh.
 Our company has a creative & innovative sourcing strategies and act as a catalyst to facilitate product sourcing requirements of importers across the globe. We are a renowned Buying Agent & Exporter Vision to setup a new trend through service and commitment.

In today’s competitive business environment, buying quality products at cheap prices is basic formula for a company to maintain global existence. We, at s m trade international, with an extensive & in-depth knowledge & expertise in various Bangladeshi Products and a dedicated team of quality controllers, co-ordinate & assist our esteemed clients across the globe at Sourcing & Buying of quality merchandise at the best industry prices.

The company offers services to source renowned suppliers of Exclusive Knit, Woven, Sweater, Terry Towel & Hometex Garments with fashion industries made through innovative technology. We also offer food product, fresh fruit & vegetable. Stringent quality practices are adopted by our experts to ensure that only quality tested products are shipped to the buyers. 									
   								</p>
   							</div>
   						</div>   						
   					</div>
   				</div>
   				<div class="promotion-item">
   					<div class="row">
   						<div class="col-md-7 col-xs-12 col-sm-7">
   							<div class="promotion-desc">
   								<p class="text-justify">
                            Why need Us:
                           <ul>
                              <li>We are consist of highly qualified and experienced personnel who are in the trade of buying, sourcing, for more than 10 years.</li>
                              <li>Timely execution of orders.</li>
                              <li>Timely delivery with assured quality.</li>
                              <li>We are professionally sound in design anticipation and development of the desired designs.</li>
                              <li>SM Trade International  works round the clock and no bar of time difference- communication is crystal clear and self-explanatory.</li>
                              <li>We have an digitized database of the styles we have done so far and can share the designs via net.</li>
                              <li>We cater to all the needs of our clients- all sorts of embroideries- prints and woven knits fabrics are deal all in one name- smtradeint.</li>
                              <li>“Client satisfaction” is our Ultimate Motto.</li>
                              <li>Honest & Ethical Business Principles.</li>
                           </ul>
   								</p>
   							</div>
   						</div>
   						<div class="col-md-5 col-xs-12 col-sm-5">
   							<div class="promotion-img">
   								<img src="http://img-aws.ehowcdn.com/340x221p/photos.demandstudios.com/getty/article/81/145/80609557.jpg" class="img-responsive" alt="Business Promotion Image">
   							</div>
   						</div>   						   						
   					</div>
   				</div>
   				<div class="promotion-item">
   					<div class="row">
   						<div class="col-md-5 col-xs-12 col-sm-5">
   							<div class="promotion-img">
   								<img src="http://img-aws.ehowcdn.com/340x221p/photos.demandstudios.com/getty/article/81/145/80609557.jpg" class="img-responsive" alt="Business Promotion Image">
   							</div>
   						</div>
   						<div class="col-md-7 col-xs-12 col-sm-7">
   							<div class="promotion-desc">
   								<p class="text-justify">
                              Our Strength:
                           <ul>
                              <li>We are capable of executing most difficult job into a simple way.</li>
                              <li>We scrutinize prices quoted by the supplier and with our own experience of the cost structure in the country, obtain the most realistic prices from them.</li>
                              <li>Competent, capable, sincerity and honesty is key strengths of s m trade international.</li>
                              <li>Vendors are in strong networking.</li>
                              <li>We check environmental control and adherence to Labor Laws, capacity and Capability of factory.</li>
                           </ul>
   								</p>
   							</div>
   						</div>   						
   					</div>
   				</div> 
   				<div class="promotion-item">
   					<div class="row">
   						<div class="col-md-7 col-xs-12 col-sm-7">
   							<div class="promotion-desc">
   								<p class="text-justify">
                           Quality: <br>
                           SM Trade International provides the highest international standard of quality.
                           <ul>
                              <li>Aim to offer our center an effective, responsive and supportive service.</li>
                              <li>Updates product regularly to the buyers.</li>
                              <li>Quality assurance inspection of the goods.</li>
                              <li>Appropriate suitable deliveries and best freight rates.</li>
                              <li>Regular communication and co-ordination giving a best facility to our buyers.</li>
                              <li>Advice on prevention of defects, Defect analysis and factory evolution.</li>
                              <li>Facilitate co-ordinate the Buyer’s visits to Bangladesh.</li>
                              <li>Adding new software and hardware to give the best services to our customers with the new technology.</li>
                           </ul>
   								</p>
   							</div>
   						</div>
   						<div class="col-md-5 col-xs-12 col-sm-5">
   							<div class="promotion-img">
   								<img src="http://img-aws.ehowcdn.com/340x221p/photos.demandstudios.com/getty/article/81/145/80609557.jpg" class="img-responsive" alt="Business Promotion Image">
   							</div>
   						</div>   						   						
   					</div>
   				</div> 
               <div class="promotion-item">
                  <div class="row">
                     <div class="col-md-5 col-xs-12 col-sm-5">
                        <div class="promotion-img">
                           <img src="http://img-aws.ehowcdn.com/340x221p/photos.demandstudios.com/getty/article/81/145/80609557.jpg" class="img-responsive" alt="Business Promotion Image">
                        </div>
                     </div>
                     <div class="col-md-7 col-xs-12 col-sm-7">
                        <div class="promotion-desc">
                           <p class="text-justify">
                              Designing & Sourcing:
                           <ul>
                              <li>We cover complete and comprehensive Garments designing unit with talented designers.</li>
                              <li>We keep eagle eye on new development and trends in fabrics, prints, colors, embroideries, trims & laces.</li>
                              <li>We, SM Trade Gather constant design information from our buyers/buying agents, new idea and requirements of prevailing trends.</li>
                              <li>We have excellent relations with the best Garments in Bangladesh those constantly work with us on the new developments.</li>
                              <li>Well-balanced professional minded merchants, sharing equal level of understanding.</li>
                           </ul>
                           </p>
                        </div>
                     </div>                     
                  </div>
               </div>
               <div class="promotion-item">
                  <div class="row">
                     <div class="col-md-7 col-xs-12 col-sm-7">
                        <div class="promotion-desc">
                           <p class="text-justify">
                           Infrastructure:
                           <ul>
                              <li>Raw material, labor, technologies are our traditional strength.</li>
                              <li>A very strong industry in the fields of Garment products, Apparel.</li>
                              <li>A strong & advanced technological structure such as own E-mail access, copiers and fax machines, Internet access, printers, digital cameras etc.</li>
                              <li>Experienced technical and non-technical labor constitute sound supports of our infrastructure.</li>
                           </ul>
                           </p>
                        </div>
                     </div>
                     <div class="col-md-5 col-xs-12 col-sm-5">
                        <div class="promotion-img">
                           <img src="http://img-aws.ehowcdn.com/340x221p/photos.demandstudios.com/getty/article/81/145/80609557.jpg" class="img-responsive" alt="Business Promotion Image">
                        </div>
                     </div>                                          
                  </div>
               </div> 
               <div class="promotion-item">
                  <div class="row">
                     <div class="col-md-5 col-xs-12 col-sm-5">
                        <div class="promotion-img">
                           <img src="http://img-aws.ehowcdn.com/340x221p/photos.demandstudios.com/getty/article/81/145/80609557.jpg" class="img-responsive" alt="Business Promotion Image">
                        </div>
                     </div>
                     <div class="col-md-7 col-xs-12 col-sm-7">
                        <div class="promotion-desc">
                           <p class="text-justify">
                              Management: <br>
Shah Mubarak is the Managing Director of SM Trade International.
He efforts selectively & meticulously on pre-fixed, preset, well-planned & defined path ensuring hundred percent achievements. If success is characterized and necessitated by knowledge, Temper and time, Shah Mubarak was born in-built that characteristics. Mr. Shah Mujibor Rahman is the founder chairman of s m trade international. 
Shah Mubarak started his career as Managing Director of s m trade international. With his intense zeal and highly focused approach, 
Knit, Woven, Sweater, Undergarments & Towel all are his production range. He has a wide range of association to cover every sector of garments manufacturing. He also expert in creative design, marketing and system support specialist. His highest Educational level is Master of Arts in English from Dhaka University. 
He studied MBA in Apparel Merchandising from BGMEA (Bangladesh Garments Manufacturers and Exporters Associations) Institute of Fashion & Technology (BIFT).
                           </p>
                        </div>
                     </div>                     
                  </div>
               </div>                                              				  				   				
   			</div>
   		</div>
   	</div>

<?php
require_once("lib/footer.php");
?>