<?php
require_once("lib/header.php"); 
?>

   	<div class="contact">
   		<div class="row">
   			<div class="container">
   				<div class="col-md-6 col-sm-6 col-xs-12">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3650.7435600476188!2d90.37597217743938!3d23.792144454382285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1515570893383" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
   				</div>
   				<div class="col-sm-6 col-md-6 c col-xs-12">
   						<?php
   						
   						if(isset($_GET["msg"])){
						  	if($_GET["msg"]==1)
						  	{
						  		echo '<p class="text-success">Great! Message Sent.</p>';
						  	}
						  	else{
						  		echo '<p class="text-danger">Opps! Message Sending Failed</p>';
						  	}
					  	}
					  ?>
					<form method="POST" action="send_mail.php">
					  <div class="form-group">
					    <label for="name">Name</label>
					    <input type="text" class="form-control" name="name" id="name" placeholder="Type Your Name">
					  </div>
					  <div class="form-group">
					    <label for="subject">Subject</label>
					    <input type="text" name="subject" class="form-control" id="subject" placeholder="Type Subject">
					  </div>
					  <div class="form-group">
					    <label for="email">E-mail</label>
					    <input type="email" name="email" class="form-control" id="email" placeholder="Type Your E-mail">
					  </div>
					  <div class="form-group">
					    <label for="message">Message</label>
					    <textarea name="message" class="form-control" id="message" placeholder="Type Your Message" rows="7"></textarea>
					  </div>						  					  					  
					  <button type="submit" class="btn">Submit</button>
					</form>   					
   				</div>
   			</div>
   		</div>
   	</div>

<?php
require_once("lib/footer.php");
?>