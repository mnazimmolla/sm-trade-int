<?php
use App\Auth\connect;
require_once("view/admin/vendor/autoload.php");

$cat = new connect;
$categories = $cat->getCategories();


?>



<!DOCTYPE html>
<html lang="en">
  <head>
  	<!-- 
		// Designer & Developer: M Nazim Uddin Molla;
		// E-mail: mohnazimuddin@gmail.com;
		// Phone: +8801711062063, +8801515243401;
		// Facebook: www.facebook.com/nzm404;
		// Gitlab: https://gitlab.com/mnazimmolla;
		// Github: https://github.com/mnazimmolla;
  	 -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SM Trade International</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
  	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="assets/css/normalize.css" />
  	<link rel="stylesheet" type="text/css" href="assets/css/demo.css" />
  	<link rel="stylesheet" type="text/css" href="assets/css/component.css" />
  	<script src="assets/js/modernizr.custom.js"></script>
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/nav.css" rel="stylesheet">
    <link rel="icon" href="assets/img/logo/logo.png" type="image/png" sizes="16x16">
  </head>
  <body>

<div id="preloader">
  <div id="status">&nbsp;</div>
</div>

   	<div class="header">
   		<div class="row">
   			<div class="container-fluid">
   				<header>
   					<div class="header-top-left">
   						<a href="index.php"><img class="logo" src="assets/img/logo/logo.png"></a>
   					</div>
					<div class="header-top-right">
						<nav id="menu">
						  <label for="tm" id="toggle-menu">Navigation <span class="drop-icon">▾</span></label>
						  <input type="checkbox" id="tm">
						  <!-- <a href="#" style="display: inline;">Logo</a> -->
						  <ul class="main-menu clearfix">
                			<li><a href="contact.php">Contact</a></li>
                			<li><a href="profile.php">Profile</a></li>
						    <li><a href="#">Products 
						        <span class="drop-icon">▾</span>
						        <label title="Toggle Drop-down" class="drop-icon" for="sm1">▾</label>
						      </a>
						      <input type="checkbox" id="sm1">
						      <ul class="sub-menu">
						        <li>
						        	<a href="#">Item 2.1</a>
						        </li>
						        <li>
						        	<a href="product.php?br=1">Food
						            <span class="drop-icon">▾</span>
						            <label title="Toggle Drop-down" class="drop-icon" for="sm2">▾</label>
						          </a>
						          <input type="checkbox" id="sm2">
						          <ul class="sub-menu">
						          	<?php
						          		foreach ($categories as $key => $value) 
						          		{
						          			if($value['cat_id']==1)
						          			{ ?>
						          				<li><a href="products.php?ul=<?php echo $value['id_uni_cat'];?>"><?php echo $value['cat_name'];?></a></li>
						          		<?php	}
						          		}
						          	?>	

						          </ul>
						        </li>
					        	<li>
					        		<a href="product.php?br=2">Vagetable
						            <span class="drop-icon">▾</span>
						            <label title="Toggle Drop-down" class="drop-icon" for="sm2">▾</label>
						          </a>
						          <input type="checkbox" id="sm2">
						          <ul class="sub-menu">

						          	<?php
						          		foreach ($categories as $key => $value) 
						          		{
						          			if ($value['cat_id'] == 2) 
						          				{ ?>
						          				<li><a href="products.php?ul=<?php echo $value['id_uni_cat'];?>"><?php echo $value['cat_name'];?></a></li>
						          		<?php	}
						          		}
						          	?>
						            


						          </ul>
						        </li>
					        	<li>
					        		<a href="product.php?br=3">Germents
						            <span class="drop-icon">▾</span>
						            <label title="Toggle Drop-down" class="drop-icon" for="sm2">▾</label>
						          </a>
						          <input type="checkbox" id="sm2">
						          <ul class="sub-menu">

						          	<?php
						          		foreach($categories as $key => $value)
						          		{
						          			if($value['cat_id']==3)
						          			{ ?>
						          				<li><a href="products.php?ul=<?php echo $value['id_uni_cat'];?>"><?php echo $value['cat_name'];?></a></li>
						          		<?php	}
						          		}
						          	?>

						          </ul>
						        </li>					        					        
						        <li><a href="#">Item 3.4</a></li>
						      </ul>
						    </li>
                <li><a href="about-us.php">About us</a></li>
                <li><a href="index.php">Home</a></li>

						  </ul>
						</nav>
					</div>					
    			</header>
   			</div>
   		</div>
   	</div>