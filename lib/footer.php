   	<div class="footer">
   		<footer>
   			<div class="row">
   				<div class="container">
   					<div class="col-md-4 col-sm-6 col-xs-12">
   						<p class="footer-heading">contact</p>
   						<p><i class="fa fa-phone" aria-hidden="true"></i><span> +8801711062063, +8801515243401</span></p>
   						<p><i class="fa fa-map-marker" aria-hidden="true"></i><span> 965/1-A West Shewrapara <br/>
   						&nbsp &nbsp Mirpur, Dhaka-1216</span></p>
   						<p><i class="fa fa-envelope-o" aria-hidden="true"></i><span> info@smtradeinternational.com</span></p>
   						<p><i class="fa fa-globe" aria-hidden="true"></i><span> smtradeinternational.com</span></p>
   					</div>
   				</div>
   			</div>
   		</footer>
   	</div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/nav.js"></script>
    <script src="assets/js/classie.js"></script>
	<script src="assets/js/boxesFx.js"></script>
	<script>
		new BoxesFx( document.getElementById( 'boxgallery' ) );
	</script>
  <script src="assets/js/script.js"></script>
<!--   <script type="text/javascript">
    $(window).on('load', function() { 
      $('#status').fadeOut();
      $('#preloader').delay("fast").fadeOut('slow');. 
      $('body').delay("fast").css({'overflow':'visible'});
    })
  </script> -->

  </body>
</html>