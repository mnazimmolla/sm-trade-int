<?php
require_once("lib/header.php");
?>


   	<div class="clearfix">
   		
   	</div>

<div class="profile-page">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
         <p>SM Trade International has achieved immense success in the global market for the superior quality products within a short span of time. The credit for the success of the company goes to the dedicated team members who with their mutual co-ordination led the company to greater heights in international market.
         </p>
         <ul>
            <li>We offer a fascinating range of products in customized designs and shape with exotic colors and captivating patterns as per the specification of the customers. </li>
             <li>Success of s m trade international, lies in the trust and faith of our clients and we strongly believe that this can only be achieved through transparency and mutual benefit. The most significant aspect for us is to “satisfy” our clients, hence we Endeavour to achieve customer’s satisfaction & for us, each and every order is equal – be it small or big. </li>
             <li>We ensure that our reputed clients get the best product in the world that match international quality standards. We share healthy rapport with our vendors and suppliers and this helps us in providing efficient services to our clients. </li>
             <li>A professionally managed organization s m trade international has carved a niche for providing efficient services in sourcing best suppliers of Knitted Garments and Woven Garments. We introduce ourselves as an established buying agent and sourcing agent in Bangladesh bridging the gap between manufacturers and buyers. We assist our clients in sourcing quality garments from reliable sources. </li>
            <li>We are consist of highly qualified and experienced personnel who are in the trade of buying, sourcing, for more than 10 years. The services which we provide are sourcing, buying, arranging, meeting with manufacturers, suppliers, co-coordinating from sampling, production to dispatch , quality assurance in production, consolidating shipments etc. </li>
            <li>We offer our services to source the best of suppliers who are well versed & updated with market forecast enabling them to innovate the developments with usage of their strong resources & creating a unique series of said product line which is widely appreciated by clients for their unmatched quality. We are associated with C & F agents to ensure safe and sound delivery of the consignments to buyers’ premises. </li>
            <li>We are counted among the leaders in international market owing to our high quality standards, transparent services and ethical business policies. </li>
            <li>We are always assurance to our buyer best Quality, best price, and best service. We deal directly with buyers and we understand the needs of our clients. </li>
             <li>We are professionally sound in design anticipation and development of the desired designs. </li>
            <li>For us quantity is no bar even 10 pcs order is cherished and pampered with care and timely deliverance.
            </li>
            <li>SM Trade International works round the clock and no bar of time difference- communication is crystal clear and self-explanatory.
            </li>
         </ul>
         </div>
      </div>
   </div>
</div>


<?php
require_once("lib/footer.php");
?>